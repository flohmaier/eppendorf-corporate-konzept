# Konzept Zusammenschluss Eppendorf Coporate und Perspektiven


[TOC]

# Allgemeines

Dieses Konzept beschreibt die Zusammenlegung der Coporate- und der Perspektivenseite von Eppendorf in eine gemeinsame Seitenstruktur um so Doppelungen an Content aber auch die Stärkung der neuen Coporateseite zu ermöglichen. Der Bereich "Karriere" innerhalb einer Webseite ist meist der am Stärkten frequentierte, weil Menschen auf der Suche nach interessanten Arbeitgebern und beruflichen Herausforderungen sind. Umso mehr Informationen sie dabei über das eigentliche Unternehmen sammeln können umso besser. Beide Argumente: die hohe Frequentierung der Stellenseite als auch die Möglichkeit sich umfassend über ein Unternehmen informieren zu können spricht dafür, corporate.eppendorf.com und perpektiven.corporate.com in einer gemeinsamen Corporatewebseite aufgehen zu lassen.

## Domains & SSH

Die Coporatewebseite von Eppendorf soll weiterhin unter der Subdomain corporate.eppendorf.com erreichbar bleiben. Für die neu integrierte Karriereseite ist es weiterhin sinnvoll eine eigene Subdomain zu nutzen, die den User direkt auf die entsprechende Stellenunterseite leitet. Aus der Außenperspektive würden wir aber eher zu den domains karriere.eppendorf.com bzw. career.eppenedorf.com raten. Perspektiven bietet die Stellenseite weiterhin. Der Begriff ist aber für einen außenstehenden Interessenten nicht auf Anhieb logisch. 

Wir würden, gerade im Zuge des Relaunches, auch dazu raten, das bestehende SSL-Zertifikat von eppendorf.com auf die Subdomains auszuweiten. Für Googles Suchindex werden Webseiten mit gesichertem Übertragungsstandard zunehmend wichtiger und wirken sich auf den 

[^https://webmaster-de.googleblog.com/2014/08/https-als-ranking-signal.html]: 

SEO-Score aus. Eine Verbindung zu einer sicheren Webseite wird für den Kunden auch außernhalb von Shops zunehmend wichtiger. Zu wissen, dass das Kontaktforumlar oder jede andere Aktivität auf einer Webseite auf einem gesicherten Server stattfindet schafft Vertrauen. Gerade ein Relaunch ermöglicht an dieser Stelle Eingriffe, weil die Umstellung von http:// auf https:// kurzzeitig im Bereich von SEO zu kleineren Einbußen führen kann (da die Inhalte ja von Google neu gecrawlt, verarbeitet und bewertet werden müssen). Auf mittelfristige Sicht zahlt sich dieses "SEO-Tief" aber aus. Da gerade ein Relaunch und die dazugehörende Neuordnung der Domainstruktur und Inhalte sowieso zu einer neuen Bewertung durch Google führt, ist es hier sinnvoll auch diesen Schritt gleich mit durchzuführen.

## Sitemapstuktur

Die neu in die Corporateseite einzugliedernde Karriereseite nimmt, aufgrund Ihrer Wichtigkeit für den Besucher, im Menü an zweiter Stelle hinter "Unternehmen" und vor "Produkte"ihren Platz ein. Dies ist deshalb wichtig, da Auswertungen anderer Coporate Seiten deutlich zeigen, dass der Bereich "Karriere" der am stärksten frequentierte ist. Aus diesem Grund sollten die Seiten zum Thema Karriere so weit nach vorne im Menü wie möglich.

Die restliche Seitenstruktur bleibt an vielen Stellen die Gleiche, da sie überlegt, sinnvoll und auch gewachsen ist. Inhalte die zukünftig als Teaser auf Verteiler- oder der Startseite gespielert werden, brauchen auch weiterhin verlinkbare Plätze auf denen sie beheimatet sind (News, Termine, Testimonials …). Durch die Verwendung der bestehenden Tiefenstruktur ist auch gewährleistet, dass der User jeden öffentlichen Inhalt der Webseite über zwei Klicks erreichen kann.

Termine aus dem Karrierebereich werden in Zukunft im größeren "Termine & Events"- Bereich gepflegt und können über eine eigene Kategorie auf der Karriereseite angezeigt werden. Der Vortiel davon ist, dass Termine nur an einer Stelle gepflegt werden. Eine Seite gibt entsprechend alle Termine und Events aus. Karrierespezifische Termine können aber trotzdem gesondert auf einer Seite ausgegeben werden.  

Taucht auf der Sitemap ein "=1" auf, sagt dies aus, dass die Überseite und die erste Unterseite sich entsprechen.

![Sitemap](Sitemap.JPG)

## Struktur der Inhalte

Die aktuellen Seiten leiden etwas darunter, dass Texte sehr oft in einer Tabnavigation und oft zusätzlich in einem Akkordeon versteckt werden. Dabei sind die Texte in den Akkordeons oft gar nicht so lang, als das eine Klapplogik nötig wäre. Der Vorschlag unserer Seits wäre, die Tablogiken beizubehalten, und so gut es geht auf Akkordeon-Elemente zu verzichten. Zusätzlich braucht es eine Deeplink-Logik die ermöglicht einen bestimmten Tab oder einen Akkordeon-Punkt auf einer einer Seite direkt anzuspringen und zu zu laden. 

## Share This

Das Plugin "Share This", das verschiedene Optionen zum Teilen der aktuellen Seite in sozialen Netzwerken anbietet, muss entfernt werden, da es nicht mehr deutschem Datenschutz entspricht. Zusammen mit Eppendorf muss erarbeitet werden, welche Seiten und Inhalte wirklich geteilt werden müssen, um auf diesen eine datenschutzkonforme Lösung zu implementieren. 

# Definitionen

## Startseite

Die neue Startseite der Cooporate-Seite zeigt zukünftig mehr Informationen an um dem Kunden einen einfacheren Einstieg auf die diversten Unterseiten zu ermöglichen. 

### Slider

Wie bisher auch eröffnet ein Slider die Startseite. Dieses Element soll auch zukünftig genutzt werden können um bestimmte Themen über längeren Zeitraum spielen zu können. 

Der Slider besteht auch weiterhin aus einem Bild, einer Headline, einem Teaser und einer internen oder externen Verlinkung. Von Seiten des Kunden ist zu klären ob es nicht vielleicht neben der Überschrift nocht eine Sublinemöglichkeit geben sollte. 

### Teaser

Auch die drei Teaser unter dem Slider würden wir beibehalten, da sie, neben dem Menü, einen schnellen Einstieg in die Seite ermöglichen. Wenn die Themen "Über Uns", "Unsere Produkte" und "Karriere" beibehalten werden sollten, würden wir den Tausch von "Unsere Produkte" und "Karriere" vorschlagen, da diese auch im Menü die Plätze tauschen. Der Teaser arbeitet als Art Tab-Logik und zeigt bei "Über Uns" eine Headline, einen Text und einen CTA. Bei "Karriere" die wichtigsten Kernzahlen aus dem Bereich Karriere und bei "Unser Produkte" ein Bild und einen Link zu den drei Produktfeldern von Eppedorf.

### News

Auf der neuen Startseite wird die Möglichkeit geschaffen aktuelle News und Presseinformatione zu spielen. Dazu gibt es eine Art Vorschau der aktuellsten 3-6 Newsarten die aus Überschrift und Teasertext sowie Newskategorie (News oder Pressemitteilung - in Zukunft auch mehr denkbar) besteht. Optimal wäre hier natürlich noch ein Bild. Da bisher bei News und Informationen aber keine Bilder gepflegt wurden ist dieser Punkt zu überdenken. Da die Häufigkeit von News und Pressemitteilungen relativ gering ist, empfehlen wir nicht mehr als sechs dieser Newsteaser zu spielen, da die Inhalte sonst schnell überholt und alt wirken. 

Ein Klickt auf den jeweiligen Newsteaser öffnet die News in einem Layer und gibt dem Besucher die Möglichkeit diese auf sozialen Netzwerken zu teilen oder per E-Mail weiterzuleiten. Möglich ist auch ein eigenes Druck-Stlye-CSS um die News/Pressemitteilung ohne zusätzliche und störende Informationen ausdrucken zu können. 

Ein Link zu allen News/Pressemitteilungen führt den User auf die News-Unterseite auf der auch die älteren Informationen aufzufinden sind. 

### Termine

Die Startseite zeigt die aktuellen 3 Termine als Teaser an. Hierbei werden Datum und Titel der Veranstaltung ausgegeben. Bei einem Klick auf den entsprechenden Termin öffnet sich ein Layer der die weiteren Beschreibungen, Ort, Mehr Informationen, Links und Bilder enthält. Sinnvoll wäre es, durch die Zusammenlegung der Karriere- und Coporate-Seite auch die Termine aus dem Bereich Karriere dort auszuspielen. Hier kann gerne in der Anzeige über eine Kategorisierung oder über unterschiedliche Farben nachgedacht werden. Je nachdem, wie viele Termine auf der gesammten Seiten grundsätzlich abgebildet werden, kann ein Link von der Startseite auf die Übersicht mit mehr Terminen unter "News & Medien" führen. Sollte es bei der schlanken Anzahl von Terminen jetzt bleiben, könnte diese Unterseite auch enfallen. 



## Header, Menüs und Footer

### Metanavigation

Da vom User über alle Eppendorfseiten bereits gelernt startet die Seite mit einer schmalen Metanavigationsleiste die die Länderseiten-Auswahl sowie den Spracheumschalter und die Suche enthält. Die Leiste bleibt farbig abgesetzt und bleibt beim scrollen sticky anheften. Anstatt "Land wählen" würde ich den Begriff "Zu unseren Lämderseiten" empfehlen. 

### Navigation

**Struktur:** Die Navigitation wird entsprechend der Sitemap umgebaut. Zusätzlich empfehlen wir das Eppendorflogo links zu setzen und dann die entsprechenden Menüpunkte auftauchen zu lassen. Der Blick der Users beginnt auf einer Seite immer oben links. Dies ist also die wichtigste Stelle um dem Kunden zu vermitteln auf welcher Seite er sich befindet. Je nach Kundenwunsch darf gerne noch ein "Cooprate" als Einklinker dem Logo zugeordnet werden. 

**Verhalten:** Die Navigationsleiste wird nach dem scrollen in der Höhe schmaller und bleibt zusätzlich zur Metanavigation sticky anhaften. Da in Zukunft auf eine Galgennavigation in der linken Marginalspalte verzichtet wird, ist der dauerhafte Zugriff auf die Navigationsleiste wichtig. 

**Aufbau:** Das Flyout des Menüs wird so gestaltet, dass auf der vollen Breite bereits möglichst viele Informationen der Unterseiten gespielt werden können. Dieses Flyout sieht, je nach Menüpunkt, anders aus. Auf der Unternehmensseite z.B.: werden die entsprechenden Unterseiten dargestellt. Auf der Produkseite können bereits die Bilder und Farbwerte von "Liquid Handling" "Cell Handling" und "Sample Handling" auftauchen. Das Menü zeigt maximal drei Ebenen an. Weitere Ebenen sollten, wie bisher auch, über Teaser auf den jeweiligen Seiten gespielt werden. Da bestimmte Inhalte sich in Zukunft im Layer öffnen wird dadurch eine weitere Ebene reduziert. Die Breadcrump-Leiste gibt dem User die Sicherheit wo er sich im Moment befindet. 

### Navigation auf Mobil

Auf mobilen Geräten wird nur eine abgesetzte Leiste mit dem Eppendorf-Logo mittig und dem Menü-Icon rechts angezeigt. Klickt man auf das Menü-Icon öffnet sich das Navigationsmenü von der rechten Seite aus und gibt Zugriff auf die entsprechenden Informationen des Menüs. Ob diese Leiste auf mobilen Geräten auch sticky haften bleibt ist vom Kunden zu definieren. Hier gibt es sowohl genug Argumente dafür wie dagegen. Der Vorteil einer auch auf mobilen Geräten haftenden Leiste ist die Möglichkeit jederzeit auf das Menü zugreifen zu können. Allerdings verbraucht diese Leiste natürlich auch Platz. Gerade bei mobilen Geräten mit weniger Bildschirmhöhe kann dies zu Platzproblemen führen. Hier wäre eine Auswertung in die bisherige Gerätestatistik sinnvoll. 

Zusätzlich zu den Menüpunkten tauchen in der mobilen Navigation auch die Länderauswahl, der Sprachenumschalter sowie die Suche auf. Metanavigation und Navigation werden also zusammengefasst. 

Verwendete Bilder aus dem Desktopmenü werden in der mobilen Navigiation nicht ausgegeben. Wir würden dazu raten die Verteilerseiten auf mobilen Geräten auszusparen und direkt auf die Unterseiten zu verlinken. Dies erspart dem User Klickwege in der mobilen Navigation.

### Footer

Den Footer würden wir grundsätzlich eher wie den der aktuellen Perspektiven-Seite gestallten. Eine abgesetzte Farbfläche in dem noch einmal wichtige Seiten sowie, prominenter als jetzt, die Social-Media Icons gesetzt werden können. Dies würde auch die Social-Media-Seite unter "News & Medien" verzichtbar machen. Es kann auch darüber nachgedacht werden, ob das Kontaktformular direkt in den Footer intigriert wird. Dies ist eine strategische Entscheidung, da das Formular dann auch häufiger genutzt werden wird (nicht gestyled).



## Seitentypen

**Startseite:** Die Startseite ist ein besonderer Seitentyp und beinhaltet alle Startseitenelmente auf voller Breite

**Inhaltsseite mit Marginalspalte:** Die Mariginalspalte der Inhaltsseite sitzt, wie bei der jetztigen Perspektivenseite rechts und ermöglicht Kontextbezogende Informationen wir Downloads, Ansprechpartner oder Linkteaser zu externen oder internen Seiten. Die Marginalspalte verschwindet auf mobilen Endgeräten an das Ende der Seite.

**Inhaltsseite mit voller Breite:** zeigt alle Inhalte der "Inhaltsseite mit Marginalspalte" allerdings sitzen die Elemente mittiger und die Marginalspalte und ihre Elemente entfallen.

**Layer:** Der Layer beinhaltet alle möglichen Contentelemente der Inhaltsseite mit Marginalspalte. Mögliche Elemente der Marginalspalte werden immer unterhalb des Content gezeigt. Der Layer ermöglicht eine einfacherer Seitennavigation, da der Kunde zwar Inhalt einer weiteren Ebene sieht aber im Hintergrund auf seiner Einstiegsseite bleibt. Es muss gewährleistet sein, dass Layer eine eigene URL haben mit der sie direkt aufgerufen werden können (Deep-Linking). Layer öffnen sich immer mit einer dunkleren Fläche im Hintergrund. Wir würden davon abraten, die bisherige gepunktete Fläche zu nutzen, da diese nicht mehr zeitgemäß ist. 

Layer haben immer einen "Schließen"-Button, reagieren auf einen Klick neben den Layer und auch auf ESC als Tastaturkürzel um zur ursprünglichen Seite zurückzukehren.



## Karriereseiten

Die Startseite des Karrierebereichs gibt eine Übersicht über die wichtigsten Dinge und bleibt einigermaßen unverändert. Dort tauchen aus dem zentralen Terminmodul alle Termine auf die dem Bereich "Karriere" zugeorndet sind, es werden die fünf Einstiegsebenen gefeatured sowie der Link zur Jobbörse und die Möglichkeit sich von Testimonials inspieren zu lassen. Die Seite zeigt zusätzlich die wichtigsten Kennzahlen zu offenen Stellen und Mitarbeitern von Eppendorf.

Der englische Bereich der Seite deckt nur ein Subset der unterschiedlichen Seiten ab und verlinkt auf unterschiedliche Länderseiten mit weiteren Informationen. 

### Seitenumbau

Der Bereich "Bewerbung" wird zukünftig aus dem Seitenbaum "Jobbörse" gelöst und landet zusammen mit dem Bereich "FAQ" in einem eigenen Menübaum. Aus "Eppendorf als Arbeitgeber" wird "Wir bieten" oder "Wir bieten Ihnen" da uns intern dies freundlicher erschien als der logische Seitennamen "Eppendorf als Arbeitgeber". Die Seite mit den deutschen Standorten linkt zur entsprechenden Seite unter "Unternehmen". 
Der Bereich "Service" enfällt. Downloads und Ansprechpartner sowie Testimonials werden jeweils bei den Einstiegsseiten zugeordnet. 

Die Einstiegsseiten der unterschiedlichen Berufsarten werden "entrümpelt". Jede der Seiten bietet auf jeder seiner Unterseiten die Möglichkeit sich neu zu entscheiden. In den Marginalspalte nach anderen Einstiegsbereichen und im Contentbereich nach unterschiedlichen Einsteigsarten. Dies verwirrt den Besucher aber, weil er nach dem Treffen einer Eintscheidung sofort wieder mit den weiteren Möglichkeiten konforntiert wird. 

### Einstiegsseiten

Ziel war es, die unterschiedliche Schachtelebene der Einstiegsseiten und ihrer Auswahlmöglichkeiten zu veringern und alles auf einer Ebene abbilden zu können. Deswegen enthalten die Fachspezifischen Einstiegsseiten in Zukunft ein Keyvisual oder ein Keyvisual Slider. Der bisherige Slider mit Produktinformationen entfällt genauso wie die Marginalspalte.  Ein Beschreibungstext erklärt die jeweilige Zielgruppeneinsteigsseite darunter folgt ein Teaser, der drei unterschiedliche Einstiege in das jeweilige Berufsfeld ermöglicht. Um hier nicht auf eine neue Seite zu springen arbeiten wir hier zukünftig mit einer Tab/Akkordeonlogik. Der vierte Bereich des Teasers kann die jeweiligen Ansprechpartner anzeigen. Stellen im Tab "Direkteinstieg" öffnen sich als Layer und ermöglicht durch "Layer to Layer"-Navigation ein Sprung zwischen den unterschiedlichen Stellenangeboten. Unterhalb der Stellenangebote gibt es einen Link zur Jobbörse, welcher genau die in diesem fachspezifischen Bereich offenen Stellen ausgibt.
Unterhalb des Teasers besteht die Möglichkeit Testimonials für genau diesen fachspezifischen Bereich zu featuren.

Um dem Benutzer auf mobilen Geräten einen vereinfachten Einstieg zu bieten, würden wir einen "spielerischen Filter" vorschlagen. Die Funktionsweise ist einfach: Als Nutzer vervollständige ich einen Satz und bekomme daraus entsprechende Ergebnisse angezeit. Beispiel: "Ich interessiere mich für" und dann Möglichkeiten "einen Job", "eine Ausbildung" "ein Praktikum" … daraufhin erhalte ich die Möglichkeit den Bereich auszuwählen "Naturwissenschaften" "IT" usw. Entweder es werden mir daraufhin direkt Jobbeschreibungen angezeigt ("Ich siche einen Job im bereich Naturwissenschaften") oder ich erhalte einen Button der mich zur Seite und Tab mit mehr Informationen weiterführ ("Ich suche einen Platz für meine Abschlussarbeit im Bereich Naturwissenschaften"). Manche Optionen gibt es nur einmal: klicke ich "ich interessere mich für ein Duales Studium" zusammen, ist automatisch der Bereich "IT" ausgewählt, weil diese Option nur einmal zur Verfügung steht. 
Diese neue mobile Einstiegsseite vereinfacht die Suche der Informationen auf Smartphones erheblich. Diese Logik könnte auch auf der Seite "Jobbörse" auf mobilen Endgeräten zum Einsatz kommen. 

### Stellenbeschreibungen

Stellenbeschreibungen öffne sich zukünftig im Layer und zeigen die entsprechenden Informationen an. Der Vorteil davon: wir veringern die Seite um eine weitere Navigationsebene und bieten dem User über die rechts - links Funktion die Möglichkeit zwischen einzelnen Stellenbeschreibungen zu wechseln ohne dauernd die "Zurücktaste" bedienen zu müssen. Inhalte bleiben erhalten und werden neu arrangiert. 

### Testimonials

Die Testimonials auf der Karriereseite sind einer wichtiger Ratgeber für alle potentiellen neuen Kollegen. Die Inhalte sind stimmig, interessant und kurz genug um übernommern zu werden. Allerdings sollte an der Darstellung gearbeitet werden. Wie bisher auch würde sich auf der neuen Seite das Testimonal als Layer über der bestehenden Seite öffnen. Über Pfleiltasten rechts und links kann zwischen den einzelnen Testiminals gewechselt werden.Gepflegt werden die Testimonals über gewöhnliche Seiten mit entsprechenden Inhaltselementen. Das würde auch das Einbinden eines Videos ermöglichen, um zukünftige Testimonials mit mehr Medien anzureichern. 

An der Anordnung des Bildes, der Jobbeschreibung und der Fragen würden wir Hand anlegen. Der bestehende Content kann und soll aber weiter genutzt werden können.

Testimonials können global auf der dafür vorgesehnen Seite ausgegeben aber natürlich auch bei entsprechenden Jobbeschreibungen verlinkt werden. In diesem Fall würden Sie sich als neue Seite laden weil ein Layer sich nicht im Layer öffnen kann. 

### Jobbörse

Die Jobbörse wird mit seiner bestehenden Funktionalität nur in der Sprachauswahl "DE" ausgegeben und sollte unserer Meinung nach umstrukturiert werden. Eppendorf nutzt bei den "Einstiegsseiten" einen Logik-Mix aus Zielruppen (z.B. Naturwissenschaftler) und Jobarten (Ausbildung, Praktiukm…). Auf der Jobbörse wird aber nur die Jobart und nicht mehr auf die Zielgruppe eingegangen. Auch wenn sich der User durch eine Einsteigsseite anhand einer Zielgruppe zu dem Button "zeige mir offene Stellen" durchgearbeitet hat, dann erhält er alle Angebote und nicht die Filterung durch die er sich mühsam gearbeitet hat (was sich mit der neuen Seite ändern würde).
Wir würden deshalb empfehlen die Zielgruppen auch im Stellenfilter der Jobbörse abzubilden. Des weiteren öffenen sich Stellen zukünftig im Layer und nicht als "Printanzeige" auf einer neuen Seite. Hierfür kann die Ansicht der Stellenanzeigen bei den Einstiegsseiten genutzt werden um Dinge stringenter anzuzeigen. Hier könnte aber auch Softgarden der limitiertende Faktor sein. Entschließt sich der potentielle neue Arbeitnehmer sich zu bewerben wird er auf die Bewerbungsseite weitergeleitet. Auf diese haben wir keinen Einfluss weil sie direkt von Softgarden kommt. Hier wären vermutlich Angleichungen im Design hilfreich. 

Auf mobilen Endgeräten kann überlegt werden, ob man die Idee des "spielerischen Filters" der Einstiegsseiten verwendet um die große Filterlogik auf Smartphones etwas aufzubrechen. 



## News & Medien

Der Bereich "News & Medien" wird zu einem Newsroom umgebaut in dem einige Unterseiten aufgehen können. Zum Einsatz kommt dabei die von der b:dreizehn entwicklte Extension "Pages as News", die es ermöglicht, normale News und deren Contentelemente für die Gestaltung von Pressemeldungen und News zu nutzen. Dies ermöglicht eine reichere Medienvielfalt innerhalb der Neuigkeiten. 

Kommt man auf den Newsroom, sieht man dort auf einen Blick 12-15 unterschiedliche und aktuelle Meldungen. Über einen "Mehr laden" Knopf kann man sich verschiedene ältere Mitteilungen nachladen. Die angezeigten Mitteillungen verfügen über verschiedene Kategorien und einen Filter der diese entsprechend Auswählbar macht. Im Gegensatz zur jetztigen Seitenstruktur gäbe es einen Newsroom, der folgende Mitteilungen unterschiedlicher Kategorien zeigt: 

- News 
- Pressemitteilungen
- Videos (falls neue YouTube Videos erscheinen) - die Seite Social Media kann dann entfallen
- Magazine: neu erscheidnene Ausgaben werden wie eine News gepflegt und entsprechend mit der Kategorie verschlagwortet

News zeigen neben den unterschiedlichen Kategorien das Datum der Veröffentlichung, einen Titel, einen Teasertext und  ein Vorschaubild. Beim Klick auf dies Mitteillung öffnet sich ein Layer mit den entsprechenden Informationen die je nach News kommuniziert werden sollen. 

**Die Vorteile:** alle Arten von News werden in Zukunft an einem zentralen Ort gepflegt und in einem modernen Newsroom ausgegeben. News bestimmter Kategorien können an anderen Stellen ausgegeben werden (Startseite zeigt aktuellste 3-6 Mitteilungen). Zusätzlich werden News im Layer geöffnet und erlauben, da sie normale Seiten sind, alle verfügbaren Contentelemente was mehr Möglichkeiten bei der Erstellung erlaubt. 

Unterhalb des Newsrooms, unter der "Mehr Laden"-Funktion sind die beiden Presseansprechpartner zu finden was eine weitere Seite einspart. 

### BioNews

Speziell für die Seite BioNews gibt es auch zukünfig eine Unterseite, die über einen Newskategoriefilter alle gepflegten Ausgaben der BioNews zeigt. Mit einem Klickt öffnet sich die gewählte Aufgabe als Layer. Zusätzlich gibt die Seite die Möglichkeit die Ausgaben zu bestellen. Man könnte sich überlegen speziell für diese Seite eine Subdomain anzulegen: https://bionews.eppendorf.com/.



# Elemente

- **Breadcrump-Leiste**

  Die Breadcrump-Leiste erscheint auf jeder Seite unterhalb des Keyvisual-Elementes (wenn gepflegt) und ermöglicht dem User das Springen innerhalb seiner Klickstrecke. Vorallem bei Seiten die auf außerhalb einer gezeigten Menüebene liegt gibt die Breadcrump-Leiste Orientierung. 

- **Akkordion**

  Das Akkordion funktioniert wie die bisherige Version und ermöglicht es viel Text, und zukünftig auch Medieninhalte, zu verstecken. Durch einen Klick auf das entsprechende Akkordion-Element und dessen Headline klappt dieses auf und zeigt die Inhalte an.

- **Tab-Navigation**

  Die Tab-Navigation ermöglicht es, innerhalb einer Seite bestimmte Themen oder Inhalte in einem neuen Reiter zu spielen. Je nach verwendetem Grid in der Umsetzung gehen wir von drei bis vier möglichen Tabs nebeneinander aus. Innerhalb der Tabs können Text- und Mediencontainer liegen. Auf mobilen Geräten werden diese untereinander angezeigt. 

- **Link & CTA:**

  Links zu externen Seiten werden durch ein kleines Symbol ergänzt, dass dem User klar macht, er verlässt die Webseite. Zusätzlich gibt es die Möglichkeit in manchen Elementen einen Link als CTA-Button darzustellen. CTAs können zwei Farben haben und haben dadurch eine Hierachie. 


- **Downloadelement**

  Ein Element, dass verschiedene Dateientypen zum Download zur Verfügung stellt. Bild und PDF-Daten können ein Vorschaubild zeigen. Zusätzlich gibt es die Möglichkeit den Dowload mit Headline und/oder Teasertext zu beschreiben. 


- **Zeitstrahl Element**

  Für die Darstellung von geschichtlichen Inhalten ist es für den User freundlicher ein Element anzuschauen, dass die entsprechenden Entwicklungen und Meilensteine von Eppendorf in der vertikalen abbildet. Das Element kann Text und Bildinhalte anzeigen und startet wahlweise in der Gegenwart oder Vergangenheit. Durch scrollen nach unten kann der User die Entwicklung der Firma oder ihrer Produkte wahrnehmen.

- **Kartenelement**

  Für die internationalen Präsenzen wäre ein Modul mit einer interkativen (Google)-Karte von Vorteil. Der User könnte so auf die einzelnen Präsenzen klicken und würde in einem Layer die entsprechenden Informationen zu den internationalen Standorten von Eppendorf bekommen. Dafür ist nebem dem Element, dass die Karte darstellt auch eine Datenbank mit den entsprechenden Geo-Tags und Informationen von Nöten. Auf Mobiltelefonen entfällt die Karte und es wird nur eine Listendarstellung der internationelen Präsenzen ausgegeben. Für die Übersicht der nationalen Präsenzen auf der Karriereseite kann dieses Element mit einer Deutschlandkarte genutzt werden. Die Infos zum Standort, den Produkten und den Ansprechpartnern würden dann in einem Layer angezeigt werden.

- **Ansprechpartnerkarte**

  Für die Ausgabe an verschiedenen Stellen der Webseite braucht es Ansprechpartnerkarten die mit den nötigen Informationen wie Name, Titel und Kontaktdaten ausgestattet sind. Diese Karten können im Content oder der Mariginalspalte gesetzt werden und werden zentral gepflegt und nur entsprechend eingebunden. Dies ermöglicht, dass Ansprechpartner und ihre Daten nur an einer Stelle geändert werden müssen. Von Seiten Eppendorfs ist zu entscheiden ob in Zukunft auch Bilder der Ansprechpartner mit ausgegeben werden. Dies ermöglicht dem Besucher einen Eindruck von seinem Gesprächsparter zu bekommen. 

- **Text/Media-Element:**

  Das Text/Media-Elemtn ist das Standardelement um Texte, Überschriften und Medieninhalte wie Fotos und/oder Videos auf der Seite einbinden zu können. Zur Auswahl stehen verschiedene Headline-Gruppen sowie Anzeigeformate der Medien (100% Breite, 50% Breite oder 33% Breite jeweils links- oder rechtsbündig). Dieses Element enthält auch Aufzählungen mit Nummern und Zeichen. 

- **Teaserboxen mit Layer**

  Für verschiedene Bereiche der Webseite bieten sich Teaserboxen mit Bild, Überschrift und Teasertext an die sich, je nach Auflösung nebeneinander und untereinander anordnen. Klickt man auf den Teaser öffnet sich ein Layer, der eine Seite mit entsprechenden Informationen zeigt. Dies ist besonders sinnvoll für die Seiten "Ökologie und Ökonomie" sowie "Soziales Engagement" macht aber auch auf anderen Seiten Sinn. Der Ansatz verhindert, dass der User sich wie bisher durch die Themen klicken muss. Eine "Layer to Layer"-Option ermöglicht es zwischen den Themen zu wechseln (durch Pfeile und/oder die Pfeiltasten der Tastatur) ohne zur Mutterseite zurück zu müssen. 

- **Produktteaser**

  Für die Produktfelder wäre ein ähnliches System wie bei den Teaserboxen sinnvoll. Damit der Kunde nicht mehrere Reihen mit Produkten durchklicken muss wäre eine Kachelübersicht mit Produktbild, Titel und Link zum externen Shop sinnvoll. 

- **Keyvisual**

  Alle Unterseiten haben ein Keyvisualbild das über die volle Breite geht und eine fixe Höhe hat. (Variante wie jetzt auf der Cooporate-Seite)

- **Keyvisual-Slider**

  Eine Version des Keyvisuals ist eine Slidervariante mit größerer Höhe und der Möglichkeit von wechselnden CTAs.

- **HTML-Modul für Graphen**

  Für die Seite "Kennzahlen" braucht es eine responsive Lösung für die Visualisierung von Geschäftszahlen. Da eine eigene Programmierung in diesem Bereich teuer und aufwndig wäre empfehlen wir einen Account bei https://infogr.am/ oder https://baremetrics.com/ und dort die Möglichkeit entsprechende Graphen als iFrame repsonsive einzubinden. 


# Wireframes

Sind hier zu finden: https://owncloud.stgt.b13.de/index.php/s/nFjpKfAuhlCfpZx

Passwort: macroto77