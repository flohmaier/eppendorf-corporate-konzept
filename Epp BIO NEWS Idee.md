# Epp BIO NEWS Idee



**Grundsätzliche Idee: Wir holen die Inhalte der Magazinen online und verwerten sie weiter um das Printmagazin als digital Veröffentlichung für verschieden Endgräte anzubieten.** 

Herausforderung: Als Print oder digitales Magazin ist es keine Problem, dass es nur 2 mal im Jahr erscheint, da der Leser die neue Ausgabe zugeschickt bekommt oder eine Benachrichtigung erhält, sobald sie verfügbar ist. Wenn wir die Artikel online anbieten, machen solche zeitlichen Abstände wenige Sinn, da die Leser Anreize brauchen um die Seite regelmäßig zu besuchen. 

## Idee

Unsere Idee ist die BIO News auf zwei weiteren, digitalen Kanälen zu veröffentlichen. Zum einen als Publikation in From einer Magazin-App und zum Anderen als ein Web Magazin. 


### Web Magazin

Für die Web-Varianten wollen wir eine Art Plattform schaffen, auf der alle bisher veröffentlichten Artikel zu finden sind, und alle zukünftigen Artikel der BIO NEWS veröffentlich werden.  Die Artikel sollen in anderer Weiße für den Leser zugänglich macht, als eine lineare Veröffentlichung oder die reine Abbildung der (Print)Publikation das mögliche machen.

Dadurch das die Inhalte der BIO NEWS eine hohe Zeitbeständigkeit haben und über längere Zeiträume relevant bleiben, können wir die große Menge an bereits bestehenden Artikeln nutzen. Durch eine regelmäßige Aktualisierung der Inhalte auf der Startseite,  gibt es für den Leser immer neue Artikel oder Zugänge zu Themen zu entdecken, wodurch ein Anreiz für den Leser entsteht, zur Site zurück zu kehren.  

Dazu gibt es verschieden Möglichkeiten. Es könnten alle Artikel zu bestimmten Themenbereichen ausgewählt werden,  beispielsweise zu verschiedene Anlässen, wie die Eppendorf-Forschungspreise, neue Erkenntnisse in Forschungsfelder  oder alle Artikel die, die Anwendung von bestehenden oder neu vorgestellten Produkten behandeln.

Erscheint ein neues Print-Magazin, wird der Erscheinung ein Bereich auf der Startseite gewidmet, der die Highlight vorstellt und auf die Web-Versionen der Artikel verlinkt. Zusätzlich kann hier auch auf die Bestellung des Print-Magazins und/oder die Angebote der digitalen Ausgaben vereisen werden. Weiterhin ist auch eine Issue-Darstellung mit allen Artikeln und Inhalten einer Issue möglich.

Weiterhin können wir die App Notes in das neuen Web Magazin integrieren. Sie wären wie andere Artikel aufrufbar und es lässt sich entweder ein Zusammenfassung oder den gesamte Inhalte der App Note online darstellen, sowie eine Möglichkeit zum PDF download am Ende der App Note. Die häufige und regelmäßige Erscheinung neuer App Notes erhöht den Zusatznutzen des Web Magzins, da diese wertvollen Inhalte außerhalb des Veröffentlichungszyklus des (Print) Magazins hinzukommen. 

Auch das Konzept der heraustrenn- und abheftbaren App Notes in den BIO NEWS liese sich online übertragen durch eine Favoriten-Funktion. Diese lässt den Leser für ihn interessante Artikel und App Notes markieren. Auf seiner Favoriten-Liste kann er die Artikel und App Notes verwalten und nach Thema, Datum oder Wichtigkeit sortieren. 
*(Anmerkung Flix: Gut nutzbar in Kombination mit dem "My Eppendorf Account")*

Auf Basis einer solchen Liste und/oder den explizit genannten Interesse eines Lesers ist es auch möglich die Artikel und App Notes auf in zugeschnitten auszugeben, sodass ihm immer neue Artikel und App Notes in den Bereichen vorgeschlagen werden, die ihn am meisten Interessieren. 

Es gibt eine Vielzahl von Möglichkeiten wie sich durch ein solches Web Magazine synergien für Eppendorf nutzen lassen. Es wäre zum Beispiel möglich Produkte die in einem Artikel oder einer App Notes zu Anwendung kommen am Ende des Artikels aufgelistet und verlinkt werden. Weiter könnte man auch innerhalb des Magazins eine Produktseite erstellen, die Kurzinformationen zu dem Produkt anzeigt und zum Produkt verlinkt (Shop oder Kontakt zum Kauf), sowie  darüber hinaus noch alle Case Studies,  App Notes und Artikel die in Zusammenhang mit dem Produkt stehen darunter Aufgelistet sind. Auch allgemeine Hinweise zu Handling und Best Practices sind hier denkbar. 

### App Magazin	

Die digitale App richtet sich mehr nach der Print Version, als die online Variatne. Die App bietet den den Vorteil, dass die Leser benachrichtigt wird sobald eine neue Ausgabe erscheint, diese (nach download) auf seinem Endgerät verfügbar und damit auch ohne Internet Zugang gelesen werden kann. 

Hier ist es mögliche den Veröffentlichungszyklus von zwei Issues im Jahr beizubehalten. Allerdings macht es auch Sinn dem Leser einen Mehrwert zu bieten.  Die neuen Zugänge zu den Inhalten die online verwendet werden lassen sich auch in der App spielen. So können beispielsweise produkt- oder eventbezogenen Sonderausgaben publiziert werden. 

Auch die Möglichkeit, dass Leser sich Artikel und App Notes als Favoriten markieren lässt sich hier anwenden. Die Favoriten sind dann, wie auch online, auf einer gesonderten Favoriten-Liste wiederzufinden. Es wäre auch denkbar, dass diese Listen sich zwischen dem online Magazin und der App synchronisieren. 